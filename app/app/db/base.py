# Import all the models, so that Base has them before being
# imported by Alembic
from app.db.base_class import Base  # noqa
# from app.models.recipes import Category, Recipe  # noqa
from app.models.users import User  # noqa
from app.models.recipes import Category, Recipe, Ingredient